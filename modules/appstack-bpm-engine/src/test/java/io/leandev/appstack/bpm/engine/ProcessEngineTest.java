package io.leandev.appstack.bpm.engine;

import io.leandev.appstack.bpm.config.BpmConfig;
import io.leandev.appstack.bpm.entity.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { BpmConfig.class })
@Transactional
@Rollback
public class ProcessEngineTest {

    @Autowired
    ProcessEngine processEngine;

    @Before
    public void init() throws IOException {
        processEngine.deploy("expense-claim-process.bpmn");
        processEngine.deploy("leave-request-process.bpmn");
    }

    @Test
    public void testQueryProcessModel()  {

        List<ProcessModel> processModels = processEngine.createProcessModelQuery().list();

        assertThat(processModels.size()).isEqualTo(2);
    }

    @Test
    public void testRedeploy() throws IOException {
        ProcessModel expect = processEngine.createProcessModelQuery().latestVersion().singleResult().get();

        processEngine.deploy("leave-request-process.bpmn");

        ProcessModel actual = processEngine.createProcessModelQuery().latestVersion().singleResult().get();

        assertThat(actual.getVersion()).isEqualTo(expect.getVersion());
    }

    @Test
    public void testUndeploy() throws IOException {
        final String processModelKey = "LeaveRequestProcess";

        ProcessModel processModel = processEngine.createProcessModelQuery().key(processModelKey).latestVersion().singleResult().get();

        processEngine.undeploy(processModel.getDeploymentId());

        List<ProcessModel> processModels = processEngine.createProcessModelQuery().list();

        assertThat(processModels.size()).isEqualTo(1);
    }

    @Test
    public void testUndeployCascade() throws Exception {
        final String processModelKey = "LeaveRequestProcess";
        ProcessModel processModel = processEngine.createProcessModelQuery().key(processModelKey).latestVersion().singleResult().get();
        Map<String, Object> variables = new HashMap<>();
        variables.put("days", 5);
        ProcessInstance expectProcessInstance = processEngine.startByKey(processModelKey, variables, "mayshih");

        processEngine.undeploy(processModel.deploymentId(), true);

        List<ProcessModel> processModels = processEngine.createProcessModelQuery().list();

        assertThat(processModels.size()).isEqualTo(1);

    }


    @Test
    public void testStartProcess() throws IOException {
        final String processModelKey = "LeaveRequestProcess";
        Map<String, Object> variables = new HashMap<>();
        variables.put("days", 5);
        ProcessInstance expectProcessInstance = processEngine.startByKey(processModelKey, variables, "mayshih");
        ProcessModel processModel = processEngine.createProcessModelQuery().key(processModelKey).singleResult().get();
        ProcessInstance processInstance = processEngine.createProcessInstanceQuery().id(expectProcessInstance.id()).singleResult().get();

        assertThat(processInstance.getProcessModelId()).isEqualTo(processModel.getId());
        assertThat(processInstance.key()).isEqualTo(expectProcessInstance.key());
        assertThat(processInstance.isEnded()).isFalse();
        assertThat(processInstance.isSuspended()).isFalse();

        Task approveTask = processEngine.createTaskQuery().processInstanceKey(processInstance.key()).singleResult().get();

        assertThat(approveTask.taskDefinitionKey()).isEqualTo("approve");
        assertThat(approveTask.name()).isEqualTo("Approve");
        assertThat(approveTask.variables().getAsInteger("days")).isEqualTo(5);

        assertThat(approveTask.assignee()).isNull();
        processEngine.claim(approveTask.id(), "ellisatkinson");
        approveTask = processEngine.createTaskQuery().processInstanceKey(processInstance.key()).singleResult().get();
        assertThat(approveTask.assignee()).isEqualTo("ellisatkinson");

        processEngine.unclaim(approveTask.id());
        approveTask = processEngine.createTaskQuery().processInstanceKey(processInstance.key()).singleResult().get();
        assertThat(approveTask.assignee()).isNull();

        approveTask.variables().put("approved", false);
        processEngine.complete(approveTask.id(), approveTask.variables(), "jasonlin");

        Task modifyTask = processEngine.createTaskQuery().processInstanceKey(processInstance.key()).singleResult().get();
        assertThat(modifyTask.taskDefinitionKey()).isEqualTo("modify");
        assertThat(modifyTask.name()).isEqualTo("Modify");
        assertThat(modifyTask.variables().getAsBoolean("approved")).isFalse();

        modifyTask.variables().put("days", 3);
        modifyTask.variables().put("cancelled", false);
        processEngine.complete(modifyTask.id(), modifyTask.variables(), "mayshih");

        TaskLog modifyTaskLog = processEngine.createTaskLogQuery().id(modifyTask.id()).singleResult().get();
        assertThat(modifyTaskLog.assignee()).isEqualTo(modifyTask.assignee());
        assertThat(modifyTaskLog.taskDefinitionKey()).isEqualTo(modifyTask.taskDefinitionKey());
        assertThat(modifyTaskLog.variables().getAsInteger("days")).isEqualTo(modifyTask.variables().get("days"));

        approveTask = processEngine.createTaskQuery().processInstanceKey(processInstance.key()).singleResult().get();

        assertThat(approveTask.taskDefinitionKey()).isEqualTo("approve");
        assertThat(approveTask.name()).isEqualTo("Approve");
        assertThat(modifyTask.variables().getAsBoolean("cancelled")).isFalse();
        assertThat(approveTask.variables().getAsInteger("days")).isEqualTo(3);

        approveTask.variables().put("approved", true);
        processEngine.complete(approveTask.id(), approveTask.variables(), "jasonlin");

        long numberOfTasks = processEngine.createTaskQuery().processInstanceKey(processInstance.key()).count();

        assertThat(numberOfTasks).isEqualTo(0);

        ProcessLog processLog = processEngine.createProcessLogQuery().key(processInstance.key()).singleResult().get();

        assertThat(processLog.getInitiator()).isEqualTo("mayshih");
    }


    @Test
    public void testModifyProcessInstance() throws IOException {
        final String processModelKey = "LeaveRequestProcess";
        Map<String, Object> variables = new HashMap<>();
        variables.put("days", 5);
        ProcessInstance processInstance = processEngine.startByKey(processModelKey, variables, "mayshih");

        Task approveTask = processEngine.createTaskQuery().processInstanceKey(processInstance.key()).singleResult().get();

        assertThat(approveTask.taskDefinitionKey()).isEqualTo("approve");
        assertThat(approveTask.name()).isEqualTo("Approve");
        assertThat(approveTask.variables().getAsInteger("days")).isEqualTo(5);

        processEngine.createProcessInstanceModification(processInstance.id())
                .startBeforeActivity("modify")
                .cancelAllForActivity("approve")
                .setVariable("days", 3)
                .execute();

        Task modifyTask = processEngine.createTaskQuery().processInstanceKey(processInstance.key()).singleResult().get();
        assertThat(modifyTask.taskDefinitionKey()).isEqualTo("modify");
        assertThat(modifyTask.name()).isEqualTo("Modify");
        assertThat(modifyTask.variables().getAsInteger("days")).isEqualTo(3);
    }
}