<?xml version="1.0" encoding="UTF-8"?>
<bpmn:definitions xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:camunda="http://camunda.org/schema/1.0/bpmn" id="Definitions_1" targetNamespace="http://bpmn.io/schema/bpmn" exporter="Camunda Modeler" exporterVersion="3.1.0">
  <bpmn:process id="ExpenseClaimProcess" name="Expense Claim Process" isExecutable="true">
    <bpmn:documentation>Business expenditure incurred by an employee should be claimed for reimbursement using an Expense Claim form. Petty cash must not be used to reimburse individual expenses. Where possible the amounts an employee will have to advance from their own personal monies should be minimised by the use of a University credit card or by arranging for the supplier to invoice the University directly.

The procedure for completing an expense claim is as follows:

    Complete the relevant expense form and attach supporting evidence of expenditure eg. receipts or bills.
    Declare on the expense form any outstanding advances.
    Ensure it is appropriately authorised either by the Head of Department or other authorised member of staff (see Authorisation of Claims).
    The department processes the expenses form on the University Financial System (UFS) and the claims will be included in the ‘Supplier’ payment run every Tuesday. The Finance Division will then pay staff members via BACS on the following Monday.
</bpmn:documentation>
    <bpmn:laneSet />
    <bpmn:startEvent id="submit" name="Submit">
      <bpmn:documentation>Sumit the expense claim request</bpmn:documentation>
      <bpmn:outgoing>SequenceFlow_19f8hcx</bpmn:outgoing>
    </bpmn:startEvent>
    <bpmn:exclusiveGateway id="ExclusiveGateway_1ddrbnz" name="Approved?">
      <bpmn:incoming>SequenceFlow_079l7hl</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_120gppg</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_1ufom5e</bpmn:outgoing>
    </bpmn:exclusiveGateway>
    <bpmn:endEvent id="approved" name="Approved">
      <bpmn:incoming>SequenceFlow_1ufom5e</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:exclusiveGateway id="ExclusiveGateway_02bgqv2" name="Cancelled?">
      <bpmn:incoming>SequenceFlow_1f05sxa</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_0e7lm81</bpmn:outgoing>
      <bpmn:outgoing>SequenceFlow_09gexfb</bpmn:outgoing>
    </bpmn:exclusiveGateway>
    <bpmn:endEvent id="cancelled" name="Cancelled">
      <bpmn:incoming>SequenceFlow_09gexfb</bpmn:incoming>
    </bpmn:endEvent>
    <bpmn:userTask id="modification" name="Modification" camunda:assignee="${initiator}">
      <bpmn:documentation>Modify the expense claim request</bpmn:documentation>
      <bpmn:incoming>SequenceFlow_120gppg</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_1f05sxa</bpmn:outgoing>
    </bpmn:userTask>
    <bpmn:userTask id="manager_approval" name="Manager Approval" camunda:assignee="${identity.supervisor(initiator)}">
      <bpmn:documentation>Approve the expense claim request</bpmn:documentation>
      <bpmn:incoming>SequenceFlow_19f8hcx</bpmn:incoming>
      <bpmn:incoming>SequenceFlow_0e7lm81</bpmn:incoming>
      <bpmn:outgoing>SequenceFlow_079l7hl</bpmn:outgoing>
    </bpmn:userTask>
    <bpmn:sequenceFlow id="SequenceFlow_19f8hcx" sourceRef="submit" targetRef="manager_approval" />
    <bpmn:sequenceFlow id="SequenceFlow_079l7hl" sourceRef="manager_approval" targetRef="ExclusiveGateway_1ddrbnz" />
    <bpmn:sequenceFlow id="SequenceFlow_120gppg" name="No" sourceRef="ExclusiveGateway_1ddrbnz" targetRef="modification">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression" language="groovy">approved==false</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_1ufom5e" name="Yes" sourceRef="ExclusiveGateway_1ddrbnz" targetRef="approved">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression" language="groovy">approved==true</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_1f05sxa" sourceRef="modification" targetRef="ExclusiveGateway_02bgqv2" />
    <bpmn:sequenceFlow id="SequenceFlow_0e7lm81" name="No" sourceRef="ExclusiveGateway_02bgqv2" targetRef="manager_approval">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression" language="groovy">!cancelled</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
    <bpmn:sequenceFlow id="SequenceFlow_09gexfb" name="Yes" sourceRef="ExclusiveGateway_02bgqv2" targetRef="cancelled">
      <bpmn:conditionExpression xsi:type="bpmn:tFormalExpression" language="groovy">cancelled</bpmn:conditionExpression>
    </bpmn:sequenceFlow>
  </bpmn:process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_1">
    <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="ExpenseClaimProcess">
      <bpmndi:BPMNShape id="_BPMNShape_StartEvent_2" bpmnElement="submit">
        <dc:Bounds x="173" y="102" width="36" height="36" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="146" y="138" width="90" height="20" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="ExclusiveGateway_1ddrbnz_di" bpmnElement="ExclusiveGateway_1ddrbnz" isMarkerVisible="true">
        <dc:Bounds x="524" y="95" width="50" height="50" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="504" y="66" width="90" height="20" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="EndEvent_0aylqcw_di" bpmnElement="approved">
        <dc:Bounds x="687" y="102" width="36" height="36" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="660" y="138" width="90" height="20" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="ExclusiveGateway_02bgqv2_di" bpmnElement="ExclusiveGateway_02bgqv2" isMarkerVisible="true">
        <dc:Bounds x="346" y="289" width="50" height="50" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="274" y="290" width="90" height="20" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="EndEvent_1kpzcug_di" bpmnElement="cancelled">
        <dc:Bounds x="687" y="411" width="36" height="36" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="660" y="447" width="90" height="20" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="UserTask_1j45p42_di" bpmnElement="modification">
        <dc:Bounds x="499" y="274" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNShape id="UserTask_1lpz3oy_di" bpmnElement="manager_approval">
        <dc:Bounds x="321" y="80" width="100" height="80" />
      </bpmndi:BPMNShape>
      <bpmndi:BPMNEdge id="SequenceFlow_19f8hcx_di" bpmnElement="SequenceFlow_19f8hcx">
        <di:waypoint x="209" y="120" />
        <di:waypoint x="321" y="120" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="197" y="110" width="90" height="20" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_079l7hl_di" bpmnElement="SequenceFlow_079l7hl">
        <di:waypoint x="421" y="120" />
        <di:waypoint x="524" y="120" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="423" y="110" width="90" height="20" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_120gppg_di" bpmnElement="SequenceFlow_120gppg">
        <di:waypoint x="549" y="145" />
        <di:waypoint x="549" y="274" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="519" y="198" width="90" height="20" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_1f05sxa_di" bpmnElement="SequenceFlow_1f05sxa">
        <di:waypoint x="499" y="314" />
        <di:waypoint x="396" y="314" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="403" y="304" width="90" height="20" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_0e7lm81_di" bpmnElement="SequenceFlow_0e7lm81">
        <di:waypoint x="371" y="289" />
        <di:waypoint x="371" y="160" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="310" y="217" width="90" height="20" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_09gexfb_di" bpmnElement="SequenceFlow_09gexfb">
        <di:waypoint x="371" y="339" />
        <di:waypoint x="371" y="429" />
        <di:waypoint x="687" y="429" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="356" y="376" width="90" height="20" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
      <bpmndi:BPMNEdge id="SequenceFlow_1ufom5e_di" bpmnElement="SequenceFlow_1ufom5e">
        <di:waypoint x="574" y="120" />
        <di:waypoint x="687" y="120" />
        <bpmndi:BPMNLabel>
          <dc:Bounds x="582" y="95" width="90" height="20" />
        </bpmndi:BPMNLabel>
      </bpmndi:BPMNEdge>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</bpmn:definitions>
