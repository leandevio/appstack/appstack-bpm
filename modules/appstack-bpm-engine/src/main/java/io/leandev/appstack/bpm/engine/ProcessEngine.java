/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.leandev.appstack.bpm.engine;
import io.leandev.appstack.bpm.entity.ProcessInstance;
import io.leandev.appstack.bpm.entity.Task;
import io.leandev.appstack.exception.NotFoundException;
import io.leandev.appstack.logging.LogManager;
import io.leandev.appstack.logging.Logger;
import org.camunda.bpm.dmn.engine.DmnEngineConfiguration;
import org.camunda.bpm.dmn.engine.impl.DefaultDmnEngineConfiguration;
import org.camunda.bpm.engine.ProcessEngineException;
import org.camunda.bpm.engine.spring.ProcessEngineFactoryBean;
import org.camunda.bpm.engine.spring.SpringProcessEngineConfiguration;
import org.camunda.bpm.engine.spring.container.ManagedProcessEngineFactoryBean;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Optional;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ProcessEngine obj = new ProcessEngine();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class ProcessEngine {
    private static final Logger LOGGER = LogManager.getLogger(ProcessEngine.class);
    private final DataSource dataSource;
    private org.camunda.bpm.engine.ProcessEngine camundaProcessEngine;
    private RepositoryService repositoryService;
    private RuntimeService runtimeService;
    private TaskService taskService;
    private HistoryService historyService;


    public ProcessEngine(DataSource dataSource) throws Exception {
        this.dataSource = dataSource;
        this.camundaProcessEngine = buildCamundaProcessEngine(dataSource);
        this.repositoryService = new RepositoryService(this);
        this.runtimeService = new RuntimeService(this);
        this.taskService = new TaskService(this);
        this.historyService = new HistoryService(this);
    }

    private org.camunda.bpm.engine.ProcessEngine buildCamundaProcessEngine(DataSource dataSource) throws Exception {
        ProcessEngineFactoryBean factoryBean = new ManagedProcessEngineFactoryBean();
        factoryBean.setProcessEngineConfiguration(buildCamundaProcessEngineConfiguration(dataSource));
        return factoryBean.getObject();
    }

    private SpringProcessEngineConfiguration buildCamundaProcessEngineConfiguration(DataSource dataSource) {
        DefaultDmnEngineConfiguration dmnEngineConfiguration = (DefaultDmnEngineConfiguration)
                DmnEngineConfiguration.createDefaultDmnEngineConfiguration();
        dmnEngineConfiguration.setDefaultInputExpressionExpressionLanguage("groovy");

        PlatformTransactionManager txManager = new DataSourceTransactionManager(dataSource);

        SpringProcessEngineConfiguration configuration = new SpringProcessEngineConfiguration();
        configuration.setProcessEngineName("AppStack BPM Engine");
        configuration.setDataSource(dataSource);
        configuration.setTransactionManager(txManager);
        configuration.setJobExecutorActivate(false);
        configuration.setDatabaseSchemaUpdate("true");
        configuration.setDmnEngineConfiguration(dmnEngineConfiguration);
        configuration.setCreateDiagramOnDeploy(true);

        return configuration;
    }

    public String name() {
        return this.camundaProcessEngine.getName();
    }

    protected org.camunda.bpm.engine.ProcessEngine camundaProcessEngine() {
        return this.camundaProcessEngine;
    }

    public RepositoryService repositoryService() {
        return this.repositoryService;
    }

    public RuntimeService runtimeService() {
        return this.runtimeService;
    }

    public TaskService taskService() {
        return this.taskService;
    }

    public HistoryService historyService() {
        return this.historyService();
    }

    public boolean deploy(String name) throws IOException {
        return repositoryService().deploy(name);
    }

    public boolean deploy(String name, byte[] bytes) throws IOException {
        return repositoryService().deploy(name, bytes);
    }

    public boolean deploy(String name, InputStream inputStream) throws IOException {
        return repositoryService().deploy(name, inputStream);
    }

    public void undeploy(String deploymentId) {
        repositoryService().undeploy(deploymentId);
    }

    public void undeploy(String deploymentId, boolean cascade) {

        repositoryService().undeploy(deploymentId, cascade);
    }

    public ProcessModelQuery createProcessModelQuery() {
        return repositoryService.createProcessModelQuery();
    }

    public ProcessInstance start(String processModelId, Map<String,Object> variables, String initiator, String processInstanceKey) {
        return runtimeService().start(processModelId, variables, initiator, processInstanceKey);
    }

    public ProcessInstance start(String processModelId, Map<String,Object> variables, String initiator) {
        return runtimeService().start(processModelId, variables, initiator);
    }

    public ProcessInstance start(String processModelId, String initiator) {
        return runtimeService().start(processModelId, initiator);
    }


    public ProcessInstance start(String processModelId, String initiator, String processInstanceKey) {
        return runtimeService().start(processModelId, initiator, processInstanceKey);
    }

    public ProcessInstance startByKey(String processModelKey, Map<String,Object> variables, String initiator, String processInstanceKey) {
        return runtimeService().startByKey(processModelKey, variables, initiator, processInstanceKey);
    }

    public ProcessInstance startByKey(String processModelKey, Map<String,Object> variables, String initiator) {
        return runtimeService().startByKey(processModelKey, variables, initiator);
    }

    public ProcessInstance startByKey(String processModelKey, String initiator) {
        return runtimeService().startByKey(processModelKey, initiator);
    }


    public ProcessInstance startByKey(String processModelKey, String initiator, String processInstanceKey) {
        return runtimeService().startByKey(processModelKey, initiator, processInstanceKey);
    }

    public ProcessInstanceQuery createProcessInstanceQuery() {
        return runtimeService.createProcessInstanceQuery();
    }

    public ProcessLogQuery createProcessLogQuery() {
        return historyService.createProcessLogQuery();
    }

    public void suspend(String processInstanceId) {
        runtimeService().suspend(processInstanceId);
    }

    public void activate(String processInstanceId) {
        runtimeService().activate(processInstanceId);

    }

    public void cancel(String processInstanceId, String reason) {
        runtimeService().cancel(processInstanceId, reason);
    }

    public void claim(String taskId, String assignee) {
        taskService().claim(taskId, assignee);
    }


    public void unclaim(String taskId) {
        taskService().unclaim(taskId);
    }

    public void complete(String taskId, Map<String,Object> variables, String executor) {
        taskService().complete(taskId, variables, executor);
    }

    public void update(String taskId, Map<String,Object> variables) {
        Optional<Task> result = createTaskQuery().id(taskId).singleResult();
        Task task = result.orElseThrow(NotFoundException::new);
        task.variables().putAll(variables);
        taskService().update(task);
    }

    public void update(Task task) {
        taskService().update(task);
    }

    public void complete(String taskId, String executor) {
        taskService().complete(taskId, executor);
    }

    public TaskQuery createTaskQuery() {
        return taskService.createTaskQuery();
    }

    public TaskLogQuery createTaskLogQuery() {
        return historyService.createTaskLogQuery();
    }

    public ProcessInstanceModificationBuilder createProcessInstanceModification(String processInstanceId) {
        return runtimeService().createProcessInstanceModification(processInstanceId);
    }
}
