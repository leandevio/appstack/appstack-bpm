package io.leandev.appstack.bpm.engine;

public class ProcessInstanceModificationBuilder {
    private RuntimeService runtimeService;
    private ProcessInstanceModification processInstanceModification;

    public ProcessInstanceModificationBuilder(RuntimeService runtimeService, String processInstanceId) {
        this.runtimeService = runtimeService;
        this.processInstanceModification = new ProcessInstanceModification(processInstanceId);
    }

    public void execute() {
        this.runtimeService.execute(this.processInstanceModification);
    }

    public ProcessInstanceModificationBuilder startBeforeActivity(String activityId) {
        processInstanceModification.setStartBeforeActivityId(activityId);
        return this;
    }

    public ProcessInstanceModificationBuilder cancelAllForActivity(String activityId) {
        processInstanceModification.setCancelActivityId(activityId);
        return this;
    }

    public ProcessInstanceModificationBuilder setVariable(String name, Object value) {
        processInstanceModification.setVariable(name, value);
        return this;
    }
}
