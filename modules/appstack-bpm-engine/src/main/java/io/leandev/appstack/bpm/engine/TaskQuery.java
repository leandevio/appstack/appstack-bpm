/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.leandev.appstack.bpm.engine;

import io.leandev.appstack.bpm.entity.Task;
import io.leandev.appstack.search.Pageable;
import io.leandev.appstack.search.Predicate;
import io.leandev.appstack.search.Sort;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * TaskQuery obj = new TaskQuery();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class TaskQuery {
    private final TaskService taskService;
    private Predicate predicate = Predicate.empty();
    private Sort sort = Sort.unsorted();

    public TaskQuery(TaskService taskService) {
        this.taskService = taskService;
    }

    public Predicate predicate() {
        return this.predicate;
    }

    public Sort sort() {
        return this.sort;
    }

    public List<Task> list() {
        return taskService.execute(this, Optional.empty());
    }

    public List<Task> list(Pageable pageable) {
        return taskService.execute(this, pageable.toOptional());
    }

    public Optional<Task> singleResult() {
        List<Task> tasks = list();
        return tasks.size() > 0 ? Optional.of(tasks.get(0)) : Optional.empty();
    }

    public long count() {
        return taskService.count(this);
    }

    public TaskQuery id(String id) {
        this.predicate = predicate.and(Predicate.eq("id", id));
        return this;
    }

    public TaskQuery assignee(String assignee) {
        this.predicate = predicate.and(Predicate.eq("assignee", assignee));
        return this;
    }

    public TaskQuery processModelId(String processModelId) {
        this.predicate = predicate.and(Predicate.eq("processModelId", processModelId));
        return this;
    }

    public TaskQuery processModelKey(String processModelKey) {
        this.predicate = predicate.and(Predicate.eq("processModelKey", processModelKey));
        return this;
    }

    public TaskQuery processInstanceId(String processInstanceId) {
        this.predicate = predicate.and(Predicate.eq("processInstanceId", processInstanceId));
        return this;
    }

    public TaskQuery processInstanceKey(String processInstanceKey) {
        this.predicate = predicate.and(Predicate.eq("processInstanceKey", processInstanceKey));
        return this;
    }

    public TaskQuery createdAfter(Date date) {
        this.predicate = predicate.and(Predicate.gt("createdOn", date));
        return this;
    }

    public TaskQuery createdBefore(Date date) {
        this.predicate = predicate.and(Predicate.lt("createdOn", date));
        return this;
    }

    public TaskQuery dueAfter(Date date) {
        this.predicate = predicate.and(Predicate.gt("dueDate", date));
        return this;
    }

    public TaskQuery dueBefore(Date date) {
        this.predicate = predicate.and(Predicate.lt("dueDate", date));
        return this;
    }

    public TaskQuery followUpAfter(Date date) {
        this.predicate = predicate.and(Predicate.gt("followUpDate", date));
        return this;
    }

    public TaskQuery followUpBefore(Date date) {
        this.predicate = predicate.and(Predicate.lt("followUpDate", date));
        return this;
    }

    public TaskQuery orderByCreatedTime(Sort.Direction direction) {
        this.sort = sort.and(Sort.by(direction, "createdTime"));
        return this;
    }

    public TaskQuery orderByCreatedTime() {
        return this.orderByCreatedTime(Sort.Direction.ASC);
    }

    public TaskQuery orderByDueDate(Sort.Direction direction) {
        this.sort = sort.and(Sort.by(direction, "dueDate"));
        return this;
    }

    public TaskQuery orderByDueDate() {
        return this.orderByDueDate(Sort.Direction.ASC);
    }

    public TaskQuery orderByFollowUpDate(Sort.Direction direction) {
        this.sort = sort.and(Sort.by(direction, "followUpDate"));
        return this;
    }

    public TaskQuery orderByFollowUpDate() {
        return this.orderByFollowUpDate(Sort.Direction.ASC);
    }

}
