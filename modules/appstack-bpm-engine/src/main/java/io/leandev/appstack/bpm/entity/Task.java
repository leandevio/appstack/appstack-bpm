/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.leandev.appstack.bpm.entity;

import io.leandev.appstack.util.Model;

import java.util.Date;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * Task obj = new Task();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class Task {
    private String id;
    private String processInstanceId;
    private String processModelId;
    private String name;
    private String description;
    private Date createdOn;
    private Date dueDate;
    private Date followUpDate;
    private String owner;
    private String assignee;
    private boolean suspended;
    private Model variables = new Model();
    private String taskDefinitionKey;

    public String getId() {
        return id;
    }

    public String id() {
        return getId();
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public String processInstanceId() {
        return getProcessInstanceId();
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public String getProcessModelId() {
        return processModelId;
    }

    public String processModelId() {
        return getProcessModelId();
    }

    public void setProcessModelId(String processModelId) {
        this.processModelId = processModelId;
    }

    public String getName() {
        return name;
    }

    public String name() {
        return getName();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String description() {
        return getDescription();
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public Date createdOn() {
        return getCreatedOn();
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public Date dueDate() {
        return getDueDate();
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Date getFollowUpDate() {
        return followUpDate;
    }

    public Date followUpDate() {
        return getFollowUpDate();
    }

    public void setFollowUpDate(Date followUpDate) {
        this.followUpDate = followUpDate;
    }

    public String getOwner() {
        return owner;
    }

    public String owner() {
        return getOwner();
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getAssignee() {
        return assignee;
    }

    public String assignee() {
        return getAssignee();
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public boolean isSuspended() {
        return suspended;
    }

    public void setSuspended(boolean suspended) {
        this.suspended = suspended;
    }

    public Model getVariables() {
        return variables;
    }

    public Model variables() {
        return getVariables();
    }

    public void setVariables(Model variables) {
        this.variables = variables;
    }

    public String getTaskDefinitionKey() {
        return taskDefinitionKey;
    }

    public String taskDefinitionKey() {
        return getTaskDefinitionKey();
    }

    public void setTaskDefinitionKey(String taskDefinitionKey) {
        this.taskDefinitionKey = taskDefinitionKey;
    }
}
