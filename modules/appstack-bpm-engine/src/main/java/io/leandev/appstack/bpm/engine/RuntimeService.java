/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.leandev.appstack.bpm.engine;

import io.leandev.appstack.bpm.entity.ProcessInstance;
import io.leandev.appstack.logging.LogManager;
import io.leandev.appstack.logging.Logger;
import io.leandev.appstack.search.*;
import io.leandev.appstack.util.Item;
import io.leandev.appstack.util.Model;
import org.camunda.bpm.engine.runtime.ProcessInstanceModificationInstantiationBuilder;

import java.util.*;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * RuntimeService obj = new RuntimeService();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class RuntimeService {
    private static final Logger LOGGER = LogManager.getLogger(RuntimeService.class);
    private final ProcessEngine processEngine;

    public RuntimeService(ProcessEngine processEngine) {
        this.processEngine = processEngine;
    }

    private org.camunda.bpm.engine.RuntimeService camundaRuntimeService() {
        return this.processEngine.camundaProcessEngine().getRuntimeService();
    }

    public ProcessInstance start(String processModelId, Map<String, Object> variables, String initiator, String processInstanceKey) {
        variables.put("initiator", initiator);
        org.camunda.bpm.engine.runtime.ProcessInstance camundaProcessInstance =
                camundaRuntimeService().startProcessInstanceById(processModelId, processInstanceKey, variables);
        ProcessInstance processInstance = toProcessInstance(camundaProcessInstance);

        return processInstance;
    }


    public ProcessInstance start(String processModelId, String initiator, String processInstanceKey) {
        Map<String, Object> variables = new HashMap<>();
        return start(processModelId, variables, initiator, processInstanceKey);
    }

    public ProcessInstance start(String processModelId, Map<String, Object> variables, String initiator) {
        String processInstanceKey = UUID.randomUUID().toString();
        return start(processModelId, variables, initiator, processInstanceKey);
    }

    public ProcessInstance start(String processModelId, String initiator) {
        Map<String, Object> variables = new HashMap<>();
        String processInstanceKey = UUID.randomUUID().toString();
        return start(processModelId, variables, initiator, processInstanceKey);
    }

    public ProcessInstance startByKey(String processModelKey, Map<String, Object> variables, String initiator, String processInstanceKey) {
        variables.put("initiator", initiator);
        org.camunda.bpm.engine.runtime.ProcessInstance camundaProcessInstance =
                camundaRuntimeService().startProcessInstanceByKey(processModelKey, processInstanceKey, variables);

        ProcessInstance processInstance = toProcessInstance(camundaProcessInstance);
        return processInstance;
    }

    public ProcessInstance startByKey(String processModelKey, String initiator, String processInstanceKey) {
        Map<String, Object> variables = new HashMap<>();
        return startByKey(processModelKey, variables, initiator, processInstanceKey);
    }

    public ProcessInstance startByKey(String processModelKey, Map<String, Object> variables, String initiator) {
        String processInstanceKey = UUID.randomUUID().toString();
        return startByKey(processModelKey, variables, initiator, processInstanceKey);
    }

    public ProcessInstance startByKey(String processModelKey, String initiator) {
        Map<String, Object> variables = new HashMap<>();
        String processInstanceKey = UUID.randomUUID().toString();
        return startByKey(processModelKey, variables, initiator, processInstanceKey);
    }

    public void suspend(String id) {
        camundaRuntimeService().suspendProcessInstanceById(id);
    }

    public void activate(String id) {
        camundaRuntimeService().activateProcessInstanceById(id);
    }

    public void cancel(String id, String reason) {
        camundaRuntimeService().deleteProcessInstance(id, reason);
    }

    public ProcessInstanceQuery createProcessInstanceQuery() {
        return new ProcessInstanceQuery(this);
    }

    public List<ProcessInstance> execute(ProcessInstanceQuery processQuery) {
        Predicate predicate = processQuery.predicate();

        org.camunda.bpm.engine.runtime.ProcessInstanceQuery camundaQuery = camundaRuntimeService().createProcessInstanceQuery();
        this.buildCamundaQuery(camundaQuery, predicate.node());

        List<org.camunda.bpm.engine.runtime.ProcessInstance> camundaProcessInstances = camundaQuery.list();
        List<ProcessInstance> processInstances = new ArrayList<>();
        for(org.camunda.bpm.engine.runtime.ProcessInstance camundaProcessInstance : camundaProcessInstances) {
            ProcessInstance processInstance = toProcessInstance(camundaProcessInstance);
            processInstances.add(processInstance);
        }
        return processInstances;
    }

    public ProcessInstance toProcessInstance(org.camunda.bpm.engine.runtime.ProcessInstance camundaProcessInstance) {
        ProcessInstance processInstance = new ProcessInstance();
        Item<ProcessInstance> item = new Item(processInstance);
        item.copyProperties(camundaProcessInstance);
        processInstance.setKey(camundaProcessInstance.getBusinessKey());
        processInstance.setProcessModelId(camundaProcessInstance.getProcessDefinitionId());
        return processInstance;
    }

    private void buildCamundaQuery(org.camunda.bpm.engine.runtime.ProcessInstanceQuery camundaQuery, Node node) {
        if(node instanceof ComparisonNode) {
            String selector = ((ComparisonNode) node).selector();
            Object expectation = ((ComparisonNode) node).expectation();
            ComparisonOperator operator = ((ComparisonNode) node).operator();
            if (selector.equals("id") && operator.equals(ComparisonOperator.EQUAL)) {
                camundaQuery.processInstanceId((String) expectation);
            } else if (selector.equals("key") && operator.equals(ComparisonOperator.EQUAL)) {
                if (((String) expectation).contains("*")) {
                    camundaQuery.processInstanceBusinessKey((String) expectation);
                } else {
                    camundaQuery.processInstanceBusinessKeyLike((String) expectation);
                }
            } else if (selector.equals("processModelId") && operator.equals(ComparisonOperator.EQUAL)) {
                camundaQuery.processDefinitionId((String) expectation);
            } else if (selector.equals("processModelKey") && operator.equals(ComparisonOperator.EQUAL)) {
                camundaQuery.processDefinitionKey((String) expectation);
            } else if (selector.equals("suspended") && operator.equals(ComparisonOperator.EQUAL)) {
                if (expectation.equals(true)) {
                    camundaQuery.suspended();
                } else {
                    camundaQuery.active();
                }
            }
        } else if(node instanceof AndNode) {
            for(Node child : ((AndNode) node).children()) {
                buildCamundaQuery(camundaQuery, child);
            }
        }
    }

    public ProcessInstanceModificationBuilder createProcessInstanceModification(String processInstanceId) {
        return new ProcessInstanceModificationBuilder(this, processInstanceId);
    }

    public void execute(ProcessInstanceModification processInstanceModification) {
        org.camunda.bpm.engine.runtime.ProcessInstanceModificationBuilder builder = camundaRuntimeService().createProcessInstanceModification(processInstanceModification.getProcessInstanceId());
        if(processInstanceModification.getCancelActivityId()!=null) {
            builder = builder.cancelAllForActivity(processInstanceModification.getCancelActivityId());
        }

        if(processInstanceModification.getStartBeforeActivityId()!=null) {
            ProcessInstanceModificationInstantiationBuilder instantiationBuilder = builder.startBeforeActivity(processInstanceModification.getStartBeforeActivityId());
            if(processInstanceModification.getVariables().size()>0) {
                for(Map.Entry<String, Object> variable : processInstanceModification.getVariables().entrySet()) {
                    instantiationBuilder = instantiationBuilder.setVariable(variable.getKey(), variable.getValue());
                }
            }
            builder = instantiationBuilder;
        }
        builder.execute();
    }


    public void check(String id) {
//        ProcessLog processLog = processEngine.createProcessLogQuery().id(id).singleResult();
//        if(processLog!=null && processLog.getEndTime()!=null) {
            // todo - publish process instance finish event.
//            this.trigger("finish", processLog);
//        }
    }

//    public Optional<String> getActvityId(String executionId) {
//        Execution camundaExecution = camundaRuntimeService().createExecutionQuery().executionId(executionId).singleResult();
//        if(camundaExecution==null) return Optional.empty();
//
//        String activityId = ((ExecutionEntity) camundaExecution).getTaskDefinitionKey();
//        return Optional.of(activityId);
//    }
//
//    public List<String> getActiveActivityIds(String processInstanceId) {
//        return camundaRuntimeService().getActiveActivityIds(processInstanceId);
//    }

    public Model getVariables(String executionId) {
        return Model.of(camundaRuntimeService().getVariables(executionId));
    }
}
