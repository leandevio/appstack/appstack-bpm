/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.leandev.appstack.bpm.engine;

import io.leandev.appstack.bpm.entity.ProcessModel;
import io.leandev.appstack.search.*;
import io.leandev.appstack.util.Item;
import org.camunda.bpm.engine.ProcessEngineException;
import org.camunda.bpm.engine.repository.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * RepositoryService obj = new RepositoryService();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class RepositoryService {
    private final ProcessEngine processEngine;

    public RepositoryService(ProcessEngine processEngine) {
        this.processEngine = processEngine;
    }

    private org.camunda.bpm.engine.RepositoryService camundaRepositoryService() {
        return this.processEngine.camundaProcessEngine().getRepositoryService();
    }

    private byte[] getLatestResource(String name) throws IOException {
        List<Deployment> deployments = camundaRepositoryService().createDeploymentQuery().deploymentName(name).orderByDeploymentTime().desc().list();

        if(deployments.size()==0) return null;

        Deployment deployment = deployments.get(0);

        InputStream inputStream = camundaRepositoryService().getResourceAsStream(deployment.getId(), name);

        byte[] bytes = new byte[inputStream.available()];

        inputStream.read(bytes);

        inputStream.close();

        return bytes;
    }

    public boolean deploy(String name) throws IOException {
        String path = String.format("/bpm/%s", name);
        InputStream inputStream = RepositoryService.class.getResourceAsStream(path);
        boolean result = deploy(name, inputStream);
        inputStream.close();
        return result;
    }

    public boolean deploy(String name, InputStream inputStream) throws IOException {
        byte[] bytes = new byte[inputStream.available()];
        inputStream.read(bytes);
        return deploy(name, bytes);
    }

    public boolean deploy(String name, byte[] bytes) throws IOException {
        if(Arrays.equals(bytes, getLatestResource(name))) return false;

        InputStream inputStream = new ByteArrayInputStream(bytes);

        DeploymentBuilder builder = camundaRepositoryService().createDeployment();

        builder.addInputStream(name, inputStream);
        builder.name(name);
        builder.deploy();
        inputStream.close();
        return true;
    }

    public void undeploy(String deploymentId) {
        try {
            undeploy(deploymentId, false);
        } catch (Exception ex) {
            throw new ProcessEngineException(ex);
        }
    }


    public void undeploy(String deploymentId, boolean cascade) {
        try {
            camundaRepositoryService().deleteDeployment(deploymentId, cascade);
        } catch (Exception ex) {
            throw new ProcessEngineException(ex);
        }
    }


    public ProcessModelQuery createProcessModelQuery() {
        return new ProcessModelQuery(this);
    }

    public List<ProcessModel> execute(ProcessModelQuery processModelQuery) {
        Predicate predicate = processModelQuery.predicate();

        ProcessDefinitionQuery camundaQuery = camundaRepositoryService().createProcessDefinitionQuery();
        this.buildCamundaQuery(camundaQuery, predicate.node());
        List<ProcessDefinition> camundaProcessModels = camundaQuery.list();;
        List<ProcessModel> processModels = new ArrayList<>();
        for(ProcessDefinition camundaProcessModel : camundaProcessModels) {
            ProcessModel processModel = new ProcessModel();
            Item<ProcessModel> item = new Item(processModel);
            item.copyProperties(camundaProcessModel);
            processModels.add(processModel);
        }
        return processModels;
    }

    private void buildCamundaQuery(ProcessDefinitionQuery camundaQuery, Node node) {
        if(node instanceof ComparisonNode) {
            String selector = ((ComparisonNode) node).selector();
            Object expectation = ((ComparisonNode) node).expectation();
            ComparisonOperator operator = ((ComparisonNode) node).operator();
            if(selector.equals("id") && operator.equals(ComparisonOperator.EQUAL)) {
                camundaQuery.processDefinitionId((String) expectation);
            } else if(selector.equals("key") && operator.equals(ComparisonOperator.EQUAL)) {
                if(((String) expectation).contains("*")) {
                    camundaQuery.processDefinitionKey((String) expectation);
                } else {
                    camundaQuery.processDefinitionKeyLike((String) expectation);
                }
            } else if(selector.equals("name") && operator.equals(ComparisonOperator.EQUAL)) {
                if(((String) expectation).contains("*")) {
                    camundaQuery.processDefinitionName((String) expectation);
                } else {
                    camundaQuery.processDefinitionNameLike((String) expectation);
                }
            } else if(selector.equals("latestVersion") && operator.equals(ComparisonOperator.EQUAL)) {
                if(expectation.equals(true)) camundaQuery.latestVersion();
            } else if(selector.equals("active") && operator.equals(ComparisonOperator.EQUAL)) {
                if(expectation.equals(true)) camundaQuery.active();
            }
        } else if(node instanceof AndNode) {
            for(Node child : ((AndNode) node).children()) {
                buildCamundaQuery(camundaQuery, child);
            }
        }
    }
}
