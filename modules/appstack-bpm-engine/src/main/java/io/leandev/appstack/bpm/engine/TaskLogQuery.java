/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.leandev.appstack.bpm.engine;

import io.leandev.appstack.bpm.entity.TaskLog;
import io.leandev.appstack.search.Pageable;
import io.leandev.appstack.search.Predicate;
import io.leandev.appstack.search.Sort;

import java.util.List;
import java.util.Optional;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * TaskLogQuery obj = new TaskLogQuery();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class TaskLogQuery {
    private final HistoryService historyService;
    private Predicate predicate = Predicate.empty();
    private Sort sort = Sort.unsorted();

    public TaskLogQuery(HistoryService historyService) {
        this.historyService = historyService;
    }

    public List<TaskLog> list() {
       return historyService.execute(this, Optional.empty());
    }

    public List<TaskLog> list(Optional<Pageable> pageable) {
        return  historyService.execute(this, pageable);
    }

    public Optional<TaskLog> singleResult() {
        List<TaskLog> taskLogs = list();
        return taskLogs.size() > 0 ? Optional.of(taskLogs.get(0)) : Optional.empty();
    }

    public TaskLogQuery id(String id) {
        this.predicate = predicate.and(Predicate.eq("id", id));
        return this;
    }

    public TaskLogQuery processInstanceId(String processInstanceId) {
        this.predicate = predicate.and(Predicate.eq("processInstanceId", processInstanceId));
        return this;
    }

    public TaskLogQuery processInstanceKey(String processInstanceKey) {
        this.predicate = predicate.and(Predicate.eq("processInstanceKey", processInstanceKey));
        return this;
    }

    public TaskLogQuery processModleId(String processModleId) {
        this.predicate = predicate.and(Predicate.eq("processModleId", processModleId));
        return this;
    }

    public TaskLogQuery processModelKey(String processModelKey) {
        this.predicate = predicate.and(Predicate.eq("processModelKey", processModelKey));
        return this;
    }

    public TaskLogQuery assignee(String assignee) {
        this.predicate = predicate.and(Predicate.eq("assignee", assignee));
        return this;
    }

    public TaskLogQuery owner(String owner) {
        this.predicate = predicate.and(Predicate.eq("owner", owner));
        return this;
    }

    public TaskLogQuery orderByEndTime(Sort.Direction direction) {
        this.sort = sort.and(Sort.by(direction, "endTime"));
        return this;
    }

    public TaskLogQuery orderByEndTime() {
        return this.orderByEndTime(Sort.Direction.ASC);
    }

    public TaskLogQuery orderByDueDate(Sort.Direction direction) {
        this.sort = sort.and(Sort.by(direction, "dueDate"));
        return this;
    }

    public TaskLogQuery orderByDueDate() {
        return this.orderByDueDate(Sort.Direction.ASC);
    }

    public TaskLogQuery orderByFollowUpDate(Sort.Direction direction) {
        this.sort = sort.and(Sort.by(direction, "followUpDate"));
        return this;
    }

    public TaskLogQuery orderByFollowUpDate() {
        return this.orderByFollowUpDate(Sort.Direction.ASC);
    }

    public Predicate predicate() {
        return this.predicate;
    }

    public Sort sort() {
        return this.sort;
    }
}
