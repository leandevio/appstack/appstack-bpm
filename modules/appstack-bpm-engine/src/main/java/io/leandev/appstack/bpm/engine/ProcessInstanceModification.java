package io.leandev.appstack.bpm.engine;

import io.leandev.appstack.util.Model;

public class ProcessInstanceModification {
    private String processInstanceId;
    private String startBeforeActivityId;
    private String cancelActivityId;
    private Model variables = new Model();

    protected ProcessInstanceModification(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public String getStartBeforeActivityId() {
        return startBeforeActivityId;
    }

    public void setStartBeforeActivityId(String startBeforeActivityId) {
        this.startBeforeActivityId = startBeforeActivityId;
    }

    public String getCancelActivityId() {
        return cancelActivityId;
    }

    public void setCancelActivityId(String cancelActivityId) {
        this.cancelActivityId = cancelActivityId;
    }

    public void setVariable(String name, Object value) {
        variables.put(name, value);
    }

    public Model getVariables() {
        return this.variables;
    }
}
