/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.leandev.appstack.bpm.engine;

import io.leandev.appstack.bpm.entity.ProcessModel;
import io.leandev.appstack.search.Predicate;
import io.leandev.appstack.search.Sort;

import java.util.List;
import java.util.Optional;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ProcessModelQuery obj = new ProcessModelQuery();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class ProcessModelQuery {
    private final RepositoryService repositoryService;
    private Predicate predicate = Predicate.empty();
    private Sort sort = Sort.unsorted();
    
    protected ProcessModelQuery(RepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }

    public List<ProcessModel> list() {
        return this.repositoryService.execute(this);
    }

    public Optional<ProcessModel> singleResult() {
        List<ProcessModel> processModels = list();
        return processModels.size() > 0 ? Optional.of(processModels.get(0)) : Optional.empty();
    }


    public ProcessModelQuery id(String id) {
        this.predicate = predicate.and(Predicate.eq("id", id));
        return this;
    }

    public ProcessModelQuery latestVersion() {
        this.predicate = predicate.and(Predicate.eq("latestVersion", true));
        return this;
    }

    public ProcessModelQuery key(String key) {
        this.predicate = predicate.and(Predicate.eq("key", key));
        return this;
    }

    public ProcessModelQuery name(String name) {
        this.predicate = predicate.and(Predicate.eq("name", name));
        return this;
    }

    public ProcessModelQuery active() {
        this.predicate = predicate.and(Predicate.eq("active", true));
        return this;
    }

    public Predicate predicate() {
        return this.predicate;
    }

    public Sort sort() {
        return this.sort;
    }

}
