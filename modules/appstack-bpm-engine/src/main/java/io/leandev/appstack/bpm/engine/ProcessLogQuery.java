/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2018 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.leandev.appstack.bpm.engine;

import io.leandev.appstack.bpm.entity.ProcessLog;
import io.leandev.appstack.search.Predicate;
import io.leandev.appstack.search.Sort;

import java.util.List;
import java.util.Optional;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * ProcessLogQuery obj = new ProcessLogQuery();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class ProcessLogQuery {
    private final HistoryService historyService;
    private Predicate predicate = Predicate.empty();
    private Sort sort = Sort.unsorted();

    public ProcessLogQuery(HistoryService historyService) {
        this.historyService = historyService;
    }

    public List<ProcessLog> list() {
        return historyService.execute(this);
    }

    public Optional<ProcessLog> singleResult() {
        List<ProcessLog> processLogs = list();
        return processLogs.size() > 0 ? Optional.of(processLogs.get(0)) : Optional.empty();
    }

    public ProcessLogQuery id(String id) {
        this.predicate = predicate.and(Predicate.eq("id", id));
        return this;
    }

    public ProcessLogQuery key(String key) {
        this.predicate = predicate.and(Predicate.eq("key", key));
        return this;
    }

    public ProcessLogQuery processModelId(String processModelId) {
        this.predicate = predicate.and(Predicate.eq("processModelId", processModelId));
        return this;
    }


    public ProcessLogQuery processModelKey(String processModelKey) {
        this.predicate = predicate.and(Predicate.eq("processModelKey", processModelKey));
        return this;
    }

    public Predicate predicate() {
        return this.predicate;
    }

    public Sort sort() {
        return this.sort;
    }
}
