package io.leandev.appstack.bpm.engine;

import io.leandev.appstack.bpm.entity.ProcessLog;
import io.leandev.appstack.bpm.entity.TaskLog;
import io.leandev.appstack.logging.LogManager;
import io.leandev.appstack.logging.Logger;
import io.leandev.appstack.search.*;
import io.leandev.appstack.util.Item;
import org.camunda.bpm.engine.history.*;

import java.util.*;

public class HistoryService {
    private static final Logger LOGGER = LogManager.getLogger(HistoryService.class);
    private final ProcessEngine processEngine;

    public HistoryService(ProcessEngine processEngine) {
        this.processEngine = processEngine;
    }

    private org.camunda.bpm.engine.HistoryService camundaHistoryService() {
        return processEngine.camundaProcessEngine().getHistoryService();
    }

    public ProcessLogQuery createProcessLogQuery() {
        return new ProcessLogQuery(this);
    }

    public List<ProcessLog> execute(ProcessLogQuery processLogQuery) {
        Predicate predicate = processLogQuery.predicate();

        HistoricProcessInstanceQuery camundaQuery = camundaHistoryService().createHistoricProcessInstanceQuery();
        this.buildCamundaQuery(camundaQuery, predicate.node());

        List<HistoricProcessInstance> camundaProcessLogs = camundaQuery.list();;
        List<ProcessLog> processLogs = new ArrayList<>();
        for(HistoricProcessInstance camundaProcessLog : camundaProcessLogs) {
            processLogs.add(toProcessLog(camundaProcessLog));
        }
        return processLogs;
    }

    private ProcessLog toProcessLog(HistoricProcessInstance camundaProcessLog) {
        ProcessLog processLog = new ProcessLog();
        Item<ProcessLog> item = new Item(processLog);
        item.copyProperties(camundaProcessLog);
        processLog.setKey(camundaProcessLog.getBusinessKey());
        processLog.setProcessModelId(camundaProcessLog.getProcessDefinitionId());
        processLog.setProcessModelKey(camundaProcessLog.getProcessDefinitionKey());
        HistoricVariableInstance camundaVariable = camundaHistoryService().createHistoricVariableInstanceQuery()
                .processInstanceId(camundaProcessLog.getRootProcessInstanceId())
                .variableName("initiator")
                .singleResult();
        processLog.setInitiator(camundaVariable.getValue().toString());
        return processLog;
    }

    private void buildCamundaQuery(HistoricProcessInstanceQuery camundaQuery, Node node) {
        if(node instanceof ComparisonNode) {
            String selector = ((ComparisonNode) node).selector();
            Object expectation = ((ComparisonNode) node).expectation();
            ComparisonOperator operator = ((ComparisonNode) node).operator();
            if(selector.equals("id") && operator.equals(ComparisonOperator.EQUAL)) {
                camundaQuery.processInstanceId((String) expectation);
            } else if(selector.equals("key") && operator.equals(ComparisonOperator.EQUAL)) {
                if(((String) expectation).contains("*")) {
                    camundaQuery.processInstanceBusinessKey((String) expectation);
                } else {
                    camundaQuery.processInstanceBusinessKeyLike((String) expectation);
                }
            } else if(selector.equals("processModelId") && operator.equals(ComparisonOperator.EQUAL)) {
                camundaQuery.processDefinitionId((String) expectation);
            } else if(selector.equals("processModelKey") && operator.equals(ComparisonOperator.EQUAL)) {
                camundaQuery.processDefinitionKey((String) expectation);
            } else if(selector.equals("completed") && operator.equals(ComparisonOperator.EQUAL)) {
                if(expectation.equals(true)) camundaQuery.completed();
            } else if(selector.equals("finished") && operator.equals(ComparisonOperator.EQUAL)) {
                if(expectation.equals(true)) camundaQuery.finished();
            } else if(selector.equals("endTime")) {
                if(operator.equals(ComparisonOperator.GREATER_THAN)) {
                    camundaQuery.finishedAfter((Date) expectation);
                } else if(operator.equals(ComparisonOperator.LESS_THAN)) {
                    camundaQuery.finishedBefore((Date) expectation);
                }
            }
        } else if(node instanceof AndNode) {
            for(Node child : ((AndNode) node).children()) {
                buildCamundaQuery(camundaQuery, child);
            }
        }
    }

    public TaskLogQuery createTaskLogQuery() {
        return new TaskLogQuery(this);
    }

    public List<TaskLog> execute(TaskLogQuery taskLogQuery, Optional<Pageable> pageable) {
        Predicate predicate = taskLogQuery.predicate();
        Optional<Pageable> pageRequest = pageable.isPresent() ?
                PageRequest.of(pageable.get(),  taskLogQuery.sort()).toOptional() : pageable;
        return execute(predicate, pageRequest);
    }

    private List<TaskLog> execute(Predicate predicate, Optional<Pageable> pageable) {
        HistoricTaskInstanceQuery camundaQuery = camundaHistoryService().createHistoricTaskInstanceQuery();
        this.filterCamundaQuery(camundaQuery, predicate.node());
        Sort sort = pageable.isPresent() ? pageable.get().sort() : Sort.unsorted();
        this.sortCamundaQuery(camundaQuery, sort);

        List<HistoricTaskInstance> camundaTaskLogs = pageable.isPresent() ?
                camundaQuery.listPage(pageable.get().offsetAsInt(), pageable.get().size()) : camundaQuery.list();
        List<TaskLog> taskLogs = new ArrayList<>();
        for(HistoricTaskInstance camundaTaskLog : camundaTaskLogs) {
            taskLogs.add(toTaskLog(camundaTaskLog));
        }
        return taskLogs;
    }

    private TaskLog toTaskLog(HistoricTaskInstance camundaTaskLog) {
        TaskLog taskLog = new TaskLog();
        Item<TaskLog> item = new Item(taskLog);
        item.copyProperties(camundaTaskLog);
        taskLog.setProcessModelId(camundaTaskLog.getProcessDefinitionId());
        taskLog.setProcessModelKey(camundaTaskLog.getProcessDefinitionKey());
        List<HistoricVariableInstance> camundaVariables = camundaHistoryService().createHistoricVariableInstanceQuery()
                .executionIdIn(camundaTaskLog.getExecutionId())
                .list();
        for(HistoricVariableInstance camundaVariable : camundaVariables) {
            String name = camundaVariable.getName();
            Object value = camundaVariable.getValue();
            taskLog.variables().put(name, value);
        }
        return taskLog;
    }

    private void filterCamundaQuery(HistoricTaskInstanceQuery camundaQuery, Node node) {
        if(node instanceof ComparisonNode) {
            String selector = ((ComparisonNode) node).selector();
            Object expectation = ((ComparisonNode) node).expectation();
            ComparisonOperator operator = ((ComparisonNode) node).operator();
            if(selector.equals("id") && operator.equals(ComparisonOperator.EQUAL)) {
                camundaQuery.taskId((String) expectation);
            } else if(selector.equals("name") && operator.equals(ComparisonOperator.EQUAL)) {
                if(((String) expectation).contains("*")) {
                    camundaQuery.taskName((String) expectation);
                } else {
                    camundaQuery.taskNameLike((String) expectation);
                }
            } else if(selector.equals("processModelId") && operator.equals(ComparisonOperator.EQUAL)) {
                camundaQuery.processDefinitionId((String) expectation);
            } else if(selector.equals("processModelKey") && operator.equals(ComparisonOperator.EQUAL)) {
                camundaQuery.processDefinitionKey((String) expectation);
            } else if(selector.equals("processInstanceId") && operator.equals(ComparisonOperator.EQUAL)) {
                camundaQuery.processInstanceId((String) expectation);
            } else if(selector.equals("processInstanceKey") && operator.equals(ComparisonOperator.EQUAL)) {
                if(((String) expectation).contains("*")) {
                    camundaQuery.processInstanceBusinessKey((String) expectation);
                } else {
                    camundaQuery.processInstanceBusinessKeyLike((String) expectation);
                }
            } else if(selector.equals("assignee") && operator.equals(ComparisonOperator.EQUAL)) {
                if(((String) expectation).contains("*")) {
                    camundaQuery.taskAssignee((String) expectation);
                } else {
                    camundaQuery.taskAssigneeLike((String) expectation);
                }
            } else if(selector.equals("owner") && operator.equals(ComparisonOperator.EQUAL)) {
                if(((String) expectation).contains("*")) {
                    camundaQuery.taskOwner((String) expectation);
                } else {
                    camundaQuery.taskOwnerLike((String) expectation);
                }
            } else if(selector.equals("endTime")) {
                if(operator.equals(ComparisonOperator.GREATER_THAN)) {
                    camundaQuery.finishedAfter((Date) expectation);
                } else if(operator.equals(ComparisonOperator.LESS_THAN)) {
                    camundaQuery.finishedBefore((Date) expectation);
                }
            }
        } else if(node instanceof AndNode) {
            for(Node child : ((AndNode) node).children()) {
                filterCamundaQuery(camundaQuery, child);
            }
        }
    }
    private void sortCamundaQuery(HistoricTaskInstanceQuery camundaQuery, Sort sort) {
        for(Sort.Order order : sort.orders()) {
            String selector = order.selector();
            Sort.Direction direction = order.direction();
            if(selector.equals("endTime")) {
                camundaQuery.orderByHistoricTaskInstanceEndTime();
                if(direction.isDescending()) camundaQuery.desc();
            } else if(selector.equals("dueDate")) {
                camundaQuery.orderByTaskDueDate();
                if(direction.isDescending()) camundaQuery.desc();
            } else if(selector.equals("followUpDate")) {
                camundaQuery.orderByTaskFollowUpDate();
                if(direction.isDescending()) camundaQuery.desc();
            }
        }
    }

}
