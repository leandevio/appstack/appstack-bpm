package io.leandev.appstack.bpm.entity;

import io.leandev.appstack.util.Model;

import java.util.Date;

public class TaskLog {
    private String id;
    private String name;
    private String description;
    private Date startTime;
    private Date endTime;

    private String processInstanceId;
    private String processModelId;
    private String processModelKey;
    private String taskDefinitionKey;

    private Model variables = new Model();

    private String owner;
    private String assignee;
    private Date dueDate;
    private Date followUpDate;

    public String getId() {
        return id;
    }

    public String id() {
        return getId();
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String name() {
        return getName();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public String description() {
        return getDescription();
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartTime() {
        return startTime;
    }

    public Date startTime() {
        return getStartTime();
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public Date endTime() {
        return getEndTime();
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public String processInstanceId() {
        return getProcessInstanceId();
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public String getProcessModelId() {
        return processModelId;
    }

    public String processModelId() {
        return getProcessModelId();
    }

    public void setProcessModelId(String processModelId) {
        this.processModelId = processModelId;
    }

    public String getProcessModelKey() {
        return processModelKey;
    }

    public String processModelKey() {
        return getProcessModelKey();
    }

    public void setProcessModelKey(String processModelKey) {
        this.processModelKey = processModelKey;
    }

    public String getTaskDefinitionKey() {
        return taskDefinitionKey;
    }

    public String taskDefinitionKey() {
        return getTaskDefinitionKey();
    }

    public void setTaskDefinitionKey(String taskDefinitionKey) {
        this.taskDefinitionKey = taskDefinitionKey;
    }

    public Model getVariables() {
        return variables;
    }

    public Model variables() {
        return getVariables();
    }

    public void setVariables(Model variables) {
        this.variables = variables;
    }

    public String getOwner() {
        return owner;
    }

    public String owner() {
        return getOwner();
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getAssignee() {
        return assignee;
    }

    public String assignee() {
        return getAssignee();
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public Date dueDate() {
        return getDueDate();
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Date getFollowUpDate() {
        return followUpDate;
    }

    public Date followUpDate() {
        return getFollowUpDate();
    }

    public void setFollowUpDate(Date followUpDate) {
        this.followUpDate = followUpDate;
    }
}
