/**
 * This document is a part of the source code and related artifacts
 * for uWeaver, an open source application development framework for
 * Enterprise Application Software.
 * <p>
 * http://www.uweaver.org
 * <p>
 * Copyright 2017 Jason Lin
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.leandev.appstack.bpm.engine;

import io.leandev.appstack.bpm.entity.Task;
import io.leandev.appstack.exception.NotFoundException;
import io.leandev.appstack.logging.LogManager;
import io.leandev.appstack.logging.Logger;
import io.leandev.appstack.search.*;
import io.leandev.appstack.util.Item;
import io.leandev.appstack.util.Model;

import java.util.*;

/**
 *  *
 * The implementation provides:
 * - ...
 * - ...
 *
 * Usage:
 *
 * ```java
 * TaskService obj = new TaskService();
 * ...
 * ```
 *
 * Note: This implementation is ...
 *
 * @author Jason Lin
 * @since 1.0
 */
public class TaskService {
    private static final Logger LOGGER = LogManager.getLogger(TaskService.class);
    private final ProcessEngine processEngine;
    private final org.camunda.bpm.engine.TaskService camundaTaskService;

    public TaskService(ProcessEngine processEngine) {
        this.processEngine = processEngine;
        this.camundaTaskService = this.processEngine.camundaProcessEngine().getTaskService();
    }

    public void claim(String taskId, String assignee) {
        camundaTaskService.claim(taskId, assignee);
    }

    public void unclaim(String taskId) {
        org.camunda.bpm.engine.task.Task camundaTask = camundaTaskService.createTaskQuery().taskId(taskId).singleResult();
        camundaTask.setAssignee(null);
        camundaTaskService.saveTask(camundaTask);
    }

    public void complete(String taskId, String executor) {
        complete(taskId, new HashMap<>(), executor);
    }

    public void complete(String taskId, Map<String, Object> variables, String executor) {
        Optional<Task> result = this.createTaskQuery().id(taskId).singleResult();

        Task task = result.orElseThrow(NotFoundException::new);
        task.setAssignee(null);
        this.update(task);

        camundaTaskService.claim(taskId, executor);

        if(variables==null) {
            this.camundaTaskService.complete(taskId);
        } else {
            this.camundaTaskService.complete(taskId, variables);
        }

        // todo - publish new tasks events.

        List<Task> todos = createTaskQuery().processInstanceId(task.processInstanceId()).list();
        if(todos.size()==0) {
            processEngine.runtimeService().check(task.processInstanceId());
        }
    }

    public TaskQuery createTaskQuery() {
        return new TaskQuery(this);
    }

    public List<Task> execute(TaskQuery taskQuery, Optional<Pageable> pageable) {
        Predicate predicate = taskQuery.predicate();
        Optional<Pageable> pageRequest = pageable.isPresent() ?
                PageRequest.of(pageable.get(),  taskQuery.sort()).toOptional() : pageable;
        return execute(predicate, pageRequest);
    }

    private List<Task> execute(Predicate predicate, Optional<Pageable> pageable) {
        org.camunda.bpm.engine.task.TaskQuery camundaQuery = camundaTaskService.createTaskQuery();
        this.filterCamundaQuery(camundaQuery, predicate.node());
        Sort sort = pageable.isPresent() ? pageable.get().sort() : Sort.unsorted();
        this.sortCamundaQuery(camundaQuery, sort);

        List<org.camunda.bpm.engine.task.Task> camundaTasks = pageable.isPresent() ?
                camundaQuery.listPage(pageable.get().offsetAsInt(), pageable.get().size()) : camundaQuery.list();
        List<Task> tasks = new ArrayList<>();
        for(org.camunda.bpm.engine.task.Task camundaTask : camundaTasks) {
            Task task = new Task();
            Item<Task> item = new Item(task);
            item.copyProperties(camundaTask, "variables");
            task.setCreatedOn(camundaTask.getCreateTime());
            task.setProcessInstanceId(camundaTask.getProcessInstanceId());
            task.setProcessModelId(camundaTask.getProcessDefinitionId());
            Model variables = this.processEngine.runtimeService().getVariables(camundaTask.getExecutionId());
            task.setVariables(variables);
            tasks.add(task);
        }
        return tasks;
    }

    public long count(TaskQuery taskQuery) {
        Predicate predicate = taskQuery.predicate();
        return count(predicate);
    }

    private long count(Predicate predicate) {
        org.camunda.bpm.engine.task.TaskQuery camundaQuery = camundaTaskService.createTaskQuery();
        this.filterCamundaQuery(camundaQuery, predicate.node());
        return camundaQuery.count();
    }

    private void filterCamundaQuery(org.camunda.bpm.engine.task.TaskQuery camundaQuery, Node node) {
        if(node instanceof ComparisonNode) {
            String selector = ((ComparisonNode) node).selector();
            Object expectation = ((ComparisonNode) node).expectation();
            ComparisonOperator operator = ((ComparisonNode) node).operator();
            if(selector.equals("id") && operator.equals(ComparisonOperator.EQUAL)) {
                camundaQuery.taskId((String) expectation);
            } else if(selector.equals("name") && operator.equals(ComparisonOperator.EQUAL)) {
                if(((String) expectation).contains("*")) {
                    camundaQuery.taskName((String) expectation);
                } else {
                    camundaQuery.taskNameLike((String) expectation);
                }
            } else if(selector.equals("processModelId") && operator.equals(ComparisonOperator.EQUAL)) {
                camundaQuery.processDefinitionId((String) expectation);
            } else if(selector.equals("processModelKey") && operator.equals(ComparisonOperator.EQUAL)) {
                camundaQuery.processDefinitionKey((String) expectation);
            } else if(selector.equals("processInstanceId") && operator.equals(ComparisonOperator.EQUAL)) {
                camundaQuery.processInstanceId((String) expectation);
            } else if(selector.equals("processInstanceKey") && operator.equals(ComparisonOperator.EQUAL)) {
                if(((String) expectation).contains("*")) {
                    camundaQuery.processInstanceBusinessKey((String) expectation);
                } else {
                    camundaQuery.processInstanceBusinessKeyLike((String) expectation);
                }
            } else if(selector.equals("assignee") && operator.equals(ComparisonOperator.EQUAL)) {
                if(((String) expectation).contains("*")) {
                    camundaQuery.taskAssignee((String) expectation);
                } else {
                    camundaQuery.taskAssigneeLike((String) expectation);
                }
            } else if(selector.equals("owner") && operator.equals(ComparisonOperator.EQUAL)) {
                camundaQuery.taskOwner((String) expectation);
            } else if(selector.equals("createdOn")) {
                if(operator.equals(ComparisonOperator.GREATER_THAN)) {
                    camundaQuery.taskCreatedAfter(((Date) expectation));
                } else if(operator.equals(ComparisonOperator.LESS_THAN)) {
                    camundaQuery.taskCreatedBefore((Date) expectation);
                }
            } else if(selector.equals("dueDate")) {
                if(operator.equals(ComparisonOperator.GREATER_THAN)) {
                    camundaQuery.dueAfter(((Date) expectation));
                } else if(operator.equals(ComparisonOperator.LESS_THAN)) {
                    camundaQuery.dueBefore((Date) expectation);
                }
            } else if(selector.equals("followUpDate")) {
                if(operator.equals(ComparisonOperator.GREATER_THAN)) {
                    camundaQuery.followUpAfter(((Date) expectation));
                } else if(operator.equals(ComparisonOperator.LESS_THAN)) {
                    camundaQuery.followUpBefore((Date) expectation);
                }
            }
        } else if(node instanceof AndNode) {
            for(Node child : ((AndNode) node).children()) {
                filterCamundaQuery(camundaQuery, child);
            }
        }
    }

    private void sortCamundaQuery(org.camunda.bpm.engine.task.TaskQuery camundaQuery, Sort sort) {
        for(Sort.Order order : sort.orders()) {
            String selector = order.selector();
            Sort.Direction direction = order.direction();
            if(selector.equals("createdOn")) {
                camundaQuery.orderByTaskCreateTime();
                if(direction.isDescending()) camundaQuery.desc();
            } else if(selector.equals("dueDate")) {
                camundaQuery.orderByDueDate();
                if(direction.isDescending()) camundaQuery.desc();
            } else if(selector.equals("followUpDate")) {
                camundaQuery.orderByFollowUpDate();
                if(direction.isDescending()) camundaQuery.desc();
            }
        }
    }

    public void update(Task task) {
        org.camunda.bpm.engine.task.Task camundaTask = camundaTaskService.createTaskQuery().taskId(task.id()).singleResult();

        camundaTask.setOwner(task.owner());
        camundaTask.setAssignee(task.assignee());
        camundaTask.setFollowUpDate(task.followUpDate());
        camundaTask.setDueDate(task.dueDate());

        camundaTaskService.saveTask(camundaTask);
        camundaTaskService.setVariables(task.id(), task.variables());
    }

}
